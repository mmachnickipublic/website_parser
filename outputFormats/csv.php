<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 22/07/16
 * Time: 10:46
 */
require_once "Formatable.abstract.php";

class csv implements Formatable{
    public static function getName(){
        return "csv string";
    }

    public function convert($array, $dataSetName){
        return $this->csvConvert($array);
    }

    private function csvConvert($array){
        $string = '';
        foreach($array as $a){
            if(is_array($a)){
                $string .= "\n".$this->csvConvert($a);
            }else{
                $string .= "\"$a\",";
            }
        }
        $string = rtrim($string, ',');

        return $string;
    }
}