<?php
require_once '../ClassLoader.php';

ClassLoader::load("HTTPParamConverter");
ClassLoader::load("ParserOutput");

fix($_GET, $_SERVER['QUERY_STRING']);
$o = getOutput($_GET);

die($o);

function getOutput($params) {
    if (isset($params["service"]) == false) return null;
    
    $httpConverter = new HTTPParamConverter();
    $parserOutput = new ParserOutput();
    $service = $params["service"];
    unset($params["service"]);
    
    if (isset($params["format"])) {
        $format = $params["format"];
        unset($params["format"]);
    } else {
        $format = "html_table";
    }
    
    $stringParameters = $httpConverter->convertParams($params);  
    
    $outputView = $parserOutput->getView($service, $stringParameters, $format);
    
    if ($format == "xml") {
        return htmlspecialchars($outputView);
    } elseif($format == 'csv'){
        $tmp = tempnam('/tmp', 'parser_');
        file_put_contents($tmp, $outputView);
        header('Content-type: text');
        header("Content-disposition: attachment; filename={$service}.csv");
        readfile($tmp);
        unlink($tmp);
    }
    return $outputView;
}

//to fix incoming HTTP variables names after PHP replaced characters in them
function fix(&$target, $source, $keep = false) {                       
    if (!$source) {                                                            
        return;                                                                
    }                                                                          
    preg_match_all(                                                            
        '/                                                                     
        # Match at start of string or &                                        
        (?:^|(?<=&))                                                           
        # Exclude cases where the period is in brackets, e.g. foo[bar.blarg]
        [^=&\[]*                                                               
        # Affected cases: periods and spaces                                   
        (?:\.|%20)                                                             
        # Keep matching until assignment, next variable, end of string or   
        # start of an array                                                    
        [^=&\[]*                                                               
        /x',                                                                   
        $source,                                                               
        $matches
    );
    foreach (current($matches) as $key) {                                      
        $key    = urldecode($key);                                             
        $badKey = preg_replace('/(\.| )/', '_', $key);                         

        if (isset($target[$badKey])) {                                         
            // Duplicate values may have already unset this                    
            $target[$key] = $target[$badKey];                                  

            if (!$keep) {                                                      
                unset($target[$badKey]);                                       
            }                                                                  
        } 
    }
}
