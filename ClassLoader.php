<?php
class ClassLoader{    
    static function load($name, $path=NULL, $type=NULL){
//        if($type == null) $type = "class";
        
        if($path == null){
            $path = __DIR__."/";
        }else{
            $path = __DIR__."/{$path}";
        }
        $path = rtrim($path, "/")."/";
        $file = $path.$name;
        if($type){
            $file .= ".$type";
        }
        $file .= ".php";
        
        if(file_exists($file)){
            require_once $file;
            return $file;
        }else{
//            throw new Exception("File for class '{$name}' doesn`t exist");
        }
        return null;
    }
}
spl_autoload_register(array("ClassLoader", "load"));
