<?php
require_once 'events/Event.abstract.php';
require_once 'actions/Action.abstract.php';
require_once '../Log.php';

autoDefineEventsActions();

/**
 * Event driven parser
 * 
 * events - an occurence of a node of specified parameters (name, value, attributes)
 * actions - actions performed in case of node occurence (get node name, get node value, get parent attributes ...)
 */
class XMLParser{
    protected $DOMDoc;
    protected $events;
    protected $result;
    protected $domDocStarted;
    protected $debug;
    protected $url;
    protected $c;
    
    public function __construct($debug=false) {
        $this->debug = $debug;
        $this->domDocStarted = false;
        $this->events = array();
        $this->result = array();
        $this->url = '';
    }
    
    public function setPage($url){
        $this->DOMDoc = new DOMDocument();

        $this->DOMDoc->preserveWhiteSpace = false;
        @$this->DOMDoc->loadHTMLFile($url);
        $this->url = $url;

        return $this;
    }
    
    public function setEvent($nodeEvent, $eventVariables=NULL){
        ClassLoader::load("$nodeEvent", "XMLParser/events");
                
        $this->events[] = new $nodeEvent($eventVariables);
        
        return $this;
    }
    
    public function addAction($action, $actionVariables=NULL){
        end($this->events)->setAction($action, $actionVariables);
        
        return $this;
    }
    
    //TODO: Function append not tested
    /*
     * Appends another action to last action, merges results of both
     */
    public function append($action, $actionVariables=NULL){
        $lastAction = end($this->events)->getLastAction();
        $lastAction->appendAction($action, $actionVariables);

        return $this;
    }

    /**
     * @param DOMNode $node
     * @throws Exception on (node == null)
     *
     * Visits all nodes and tries to run events on each
     * When node is null, it runs across the whole document available
     */
    public function parse(){
        $this->parseIn();

        return $this;
    }

    /**
     * @param string $nodeId
     *
     * Parses only specified node by nodeId.
     * Visits all nodes within specified node and tries to run events on each.
     *
     */
    public function parseWithin($nodeId){
        $n = $this->DOMDoc->getElementById($nodeId);
        if($n){
            $this->parseIn($n);
        }else{
            error_log("Parser Notice: Node with id '{$nodeId}' does not exist, parsing all document", 0);
            $this->parse();
        }

        return $this;
    }

    /**
     * Results read out causes XMLParser to be reset (results, events and actions are deleted)
     * 
     * @return array results
     */
    public function fetchResults(){ 
        $r = $this->result;
        $this->purge();
        return $r;
    }
    
    public function purge(){
        unset($this->DOMDoc);
        unset($this->events);
        unset($this->result);
        $this->events = array();
        $this->result = array();
        $this->DOMDoc = null;
        $this->domDocStarted = false;
        $this->url = '';
    }

    //TODO: Improve error messages for debugging so that they doesn't cause errors and are more meaningful when parsing document ie give more precise information about where the parser actually is pointing at

    protected function parseIn($node = NULL){
        if($node == NULL){
            if($this->domDocStarted){
                throw new Exception("Node error, null node causing infinite loop. Error, exiting parsing after {$this->c} node.");
            }
            $node = $this->DOMDoc;
            $this->domDocStarted = true;
        }
        if($this->debug) {
            if ($node instanceof DOMElement) Log::write("Parsing: {$node->tagName}::" . $node->getAttribute('id') . '::' . $node->getAttribute('href'));
            if ($node instanceof DOMText) Log::write("Parsing text node: {$node->textContent}");
        }
        $this->tryEvents($node);
        if($node->hasChildNodes() == false){
            return $this;
        }
        foreach($node->childNodes as $child){
            if($child instanceof DOMText && preg_match('/ *\r?\n/', $node->textContent)){
                continue;
            }
            $this->c++;
            if($this->debug){
                if($node->nextSibling instanceof DOMElement) Log::write("Node {$node->tagName} has sibling::".$node->nextSibling->tagName.':'.$node->nextSibling->getAttribute('id'));
            }
            $this->parseIn($child);
        }
        return $this;
    }

    private function tryEvents($xmlSubTree){
        foreach($this->events as $event){
            if($event->occurrence($xmlSubTree)){
                $this->result[] = $event->performAction($xmlSubTree);                
            }
        }
    }
}

function autoDefineEventsActions(){
    $eventsPath = __DIR__."/events/";
    $actionsPath = __DIR__."/actions/";
    
    defineEach(scandir($eventsPath));
    defineEach(scandir($actionsPath));
}

function defineEach($files, $type=NULL){
    foreach($files as $file){
        if($file != ".." && $file != "." && substr($file, strpos($file, ".")) != ".abstract.php"){
            $class = rtrim(rtrim($file, ".php"), "$type");
            $class = rtrim($class, ".");
            
            define($class, $class);
        }
    }
}