<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 22/07/16
 * Time: 16:31
 */
class Collect_li_Siblings_a_href extends Action
{
    public function trigger(DOMElement $node){
        $result = array();

        foreach($node->getElementsByTagName('li') as $li){
            $a = $li->getElementsByTagName('a')->item(0);

            if($a){
                $result[] = $a->getAttribute('href');
            }
        }
        return $result;
    }
}