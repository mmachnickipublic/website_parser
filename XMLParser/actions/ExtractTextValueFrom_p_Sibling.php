<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 26/07/16
 * Time: 14:53
 */
class ExtractTextValueFrom_p_Sibling extends Action
{
    public function trigger(DOMElement $node)
    {
        $tg = $node->tagName;
        $s = $this->nextSibling($node);
        if($s && $s->tagName == 'p'){
            $t = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $s->textContent);

            return $t;
        }
        return null;
    }

    private function nextSibling($node){
        if($node->nextSibling){
            if($node->nextSibling instanceof DOMText){
                $n = $this->nextSibling($node->nextSibling);
                return $n;
            }
            return $node->nextSibling;
        }
        return NULL;
    }
}