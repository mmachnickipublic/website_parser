<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 26/07/16
 * Time: 08:58
 */
class HampshirePagesTagValues extends Action
{
    //Assumed li element containing venue data
    public function trigger(DOMElement $node)
    {
        $titleTag = $node->getElementsByTagName('h2')->item(0);
        $title = $this->purify($titleTag->textContent);
        $registractionAddressTag = $node->getElementsByTagName('address')->item(0);
        $registractionAddress = $registractionAddressTag->hasAttributes() ? $this->variable['mainUrl'].$registractionAddressTag->getAttribute('data-url') : NULL;
        $r = array();

        if($title){
            $r['name'] = $title;
        }
        if($registractionAddress){
            $r['registration url'] = $registractionAddress;
        }

        foreach($node->getElementsByTagName('p') as $p){
            if($p->hasAttributes() && $p->getAttribute('itemprop')){
                $this->extractAddress($p, $r);
                break;
            }
        }

        if(count($r)){
            return $r;
        }
        return null;
    }

    private function extractAddress($node, &$address){
        foreach($node->childNodes as $child){
            $a = $child->getAttribute('itemprop');
            if($child->hasAttributes() && $a){
                switch ($a){
                    case 'streetAddress':
                        $address['street address'] =  $this->purify($child->textContent);
                        break;
                    case 'addressLocality':
                        $address['address locality'] = $this->purify($child->textContent);
                        break;
                    case 'postalCode':
                        $address['postal code'] = $this->purify($child->textContent);
                        break;
                }
            }
        }
    }

    private function purify($txt){
        return preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $txt);
    }
}