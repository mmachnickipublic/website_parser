<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 19/07/16
 * Time: 11:38
 */
class Tag_a_Href extends Action
{
    public function trigger(DOMElement $node)
    {
        return array('href' => $node->getAttribute('href'));
    }
}