<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 19/07/16
 * Time: 12:19
 */
class CollectNextNode_ul_DataWith_a_href_Child extends Action
{
    public function trigger(DOMElement $node)
    {
        $element = $this->findNextSensibleNode($node);
        if($element instanceof DOMElement == false){
            return NULL;
        }
        return $this->collectChildrenValues($element);
    }

    private function findNextSensibleNode($node){
        $n = $node->nextSibling ;
        if($n instanceof DOMElement){
            return $n;
        }
        return $this->findNextSensibleNode($n);
    }

    private function collectChildrenValues(DOMElement $node){
        if($node->tagName != 'ul'){
            return NULL;
        }
        $v = array();
        $aLi = $node->getElementsByTagName('li');
        foreach($aLi as $li){
            $a = NULL;
            if($li->hasChildNodes()){
                $aA = $li->getElementsByTagName('a');
                $a = $aA->item(0);
            }
            if($a){
                $v[] = $a->getAttribute('href');
            }else{
                $v[] = str_replace(chr(194), ' ',$li->nodeValue);
            }
        }
        return $v;
    }
}