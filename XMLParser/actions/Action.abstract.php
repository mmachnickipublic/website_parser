<?php
//definition of all existing actions, for the ease of use (hopefully)
//define ("GetAttributes", "GetAttributes");
//define ("GetAttribute", "GetAttribute");
//define("GetParentAttribute", "GetParentAttribute");

abstract class Action{
    public $appended;

    protected $variable;
    protected $appendedActions;
    
    public function __construct($variable=NULL) {
        $this->variable = $variable;
        $this->appended = array();
        $this->appended = false;
    }
    
    abstract public function trigger(DOMElement $node);
    
    public function getName(){
        return get_class($this);
    }
    
    public function getVariable(){
        return $this->variable;
    }
    
    public function appendAction($parserAction, $actionVariables=NULL){
        ClassLoader::load("$parserAction", "XMLParser/actions");
        $this->appendedActions[] = new $parserAction($actionVariables);
        $this->appended = true;
    }
}