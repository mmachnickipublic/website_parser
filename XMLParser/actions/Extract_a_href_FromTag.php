<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 22/07/16
 * Time: 09:29
 */
class Extract_a_href_FromTag extends Action
{
    public function trigger(DOMElement $node)
    {
        $r = array();
        foreach ($node->getElementsByTagName('a') as $a) {
            $r[] = $a->getAttribute('href');
        }
        if (count($r) == 1) {
            return $r[0];
        }
        if (count($r) > 1) {
            return $r;
        }
        return null;
    }
}