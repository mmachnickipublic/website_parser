<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 21/07/16
 * Time: 15:00
 */
class ExtractTextValueFromTag extends Action
{
    public function trigger(DOMElement $node)
    {
        $text = str_replace(chr(194), ' ', $node->textContent); //dirty fix

        if(is_array($this->variable) && count($this->variable) > 0){
            $text = array($this->variable[0] => $text);
        }elseif($this->variable){
            $text = array($this->variable => $text);
        }
        return $text;
    }
}