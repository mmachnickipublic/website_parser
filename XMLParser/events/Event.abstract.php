<?php
//define("NodeName", "NodeName");
//define("NodeNameInParent", "NodeNameInParent");

abstract class Event{
    protected $actions;
    protected $variable;
    protected $lastAction;
    
    public function __construct($nodeVariables) {                
        $this->actions = array();
        $this->variable = $nodeVariables;
        $this->lastAction = NULL;
    }
    
    //abstract protected function initialise();
    abstract public function occurrence($node);    
    
    public function getName(){
        return get_class($this);
    }
    
    public function setAction($parserAction, $actionVariables=NULL){
        ClassLoader::load("$parserAction", "XMLParser/actions");
        $this->actions[] = new $parserAction($actionVariables);
        $this->lastAction = &end($this->actions);
    }

    public function getLastAction(){
        return $this->lastAction;
    }
    
    public function performAction($node){
        $result = array();
        foreach($this->actions as $action){
            $r2 = null;
            $r = $action->trigger($node);
            if($action->appended){
                $r2 = $action->trigger($node);
            }
            if(is_array($r)){
                foreach($r as $key=>$value){
                    $result[$key] = $value;
                }
                if($r2){
                    $result[] = $r2;
                }
            }else{
                if($r2){
                    $result[$r] = $r2;
                }
                $result[] = $r;
            } 
        }        
        if(count($result) > 0) return $result;
            
        return null;
    }
}