<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 20/07/16
 * Time: 14:35
 */
class TagOccurrence extends Event{

    public function occurrence($node){
        $test = $node instanceof DOMElement;
        $test = $test && $node->tagName == $this->variable['tagName'];

        return $test;
    }
}