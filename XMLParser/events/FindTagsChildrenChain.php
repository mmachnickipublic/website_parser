<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 22/07/16
 * Time: 15:48
 */

class FindTagsChildrenChain extends Event{
    public function occurrence($node){
        if($node instanceof DOMElement == false){
            return false;
        }
        $child = $node;
        foreach($this->variable as $tagName){
            $child = $child->getElementsByTagName($tagName)->item(0);
            
            if($child == NULL){
                return false;
            }
        }
        return true;
    }
}