<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 19/07/16
 * Time: 11:25
 */
class TagAttributeValueMatching extends Event{

    public function occurrence($node){
        $b = $node instanceof DOMElement;
        $b = $b && $node->tagName == $this->variable['tag'];
        $b = $b && $node->hasAttributes();
        $b = $b && preg_match($this->variable['match'], $node->getAttribute($this->variable['attribute']));

        return $b;
    }
}