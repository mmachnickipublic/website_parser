<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 21/07/16
 * Time: 15:46
 */
class Find_p_ul_PairTag extends Event{

    public function occurrence($node){
        $b = $node instanceof DOMElement;
        $b = $b && $node->nodeName == 'p';
        $nextSibling = $node->nextSibling;
        if($nextSibling instanceof DOMText && preg_match('/ *\r?\n/', $nextSibling->textContent)){
            $nextSibling = $node->nextSibling->nextSibling;
        }
        $b = $b && ($nextSibling instanceof DOMElement) && $nextSibling->nodeName == 'ul';

        return $b;
    }
}