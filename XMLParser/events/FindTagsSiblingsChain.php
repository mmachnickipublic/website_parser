<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 26/07/16
 * Time: 14:18
 */

class FindTagsSiblingsChain extends Event
{
    public function occurrence($node)
    {
        $b = $node instanceof DOMElement;

        $sibling = $node;
        foreach($this->variable as $name){
            if($sibling == NULL){
                $b = false;
                break;
            }
            $nn = $sibling->nodeName;
            $b = $b && $sibling->nodeName == $name;

            $sibling = $this->nextSibling($sibling);

            if(!$b){
                break;
            }
        }
        return $b;
    }

    private function nextSibling($node){
        if($node->nextSibling){
            if($node->nextSibling instanceof DOMText){
                $n = $this->nextSibling($node->nextSibling);
                return $n;
            }
            return $node->nextSibling;
        }
        return NULL;
    }
}