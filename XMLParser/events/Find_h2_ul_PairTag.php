<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 19/07/16
 * Time: 12:19
 */
class Find_h2_ul_PairTag extends Event{

    public function occurrence($node){
        $b = $node instanceof DOMElement;
        $b = $b && $node->nodeName == 'h2';
        $nextSibling = $node->nextSibling;
        if($nextSibling instanceof DOMText && preg_match('/ *\r?\n/', $nextSibling->textContent)){
            $nextSibling = $node->nextSibling->nextSibling;
        }
        $b = $b && ($nextSibling instanceof DOMElement) && $nextSibling->nodeName == 'ul';

        return $b;
    }
}