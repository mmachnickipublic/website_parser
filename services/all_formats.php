<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 22/07/16
 * Time: 15:04
 */
ClassLoader::load('Serviceable', 'services', 'interface');

class all_formats implements Serviceable{
    protected $dir = __DIR__;

    public function getViewData($constraints = NULL){
        $services = array();

        foreach(scandir($this->dir.'/../outputFormats') as $item){
            if($item == '..' || $item == '.' || $this->isProhibited($item)){
                continue;
            }
            $item = rtrim($item, '.php');
            $services[] = array(ucwords(str_replace('_', ' ', $item)) => $item);
        }
        return $services;
    }

    protected function isProhibited($file){
        if(is_dir($this->dir.'/../outputFormats/'.$file)){
            return true;
        }
        $prohibited = array(
            'abstract',
            'interface'
        );

        foreach($prohibited as $p){
            if(strpos($file, $p) !== false){
                return true;
            }
        }
        return false;
    }
}