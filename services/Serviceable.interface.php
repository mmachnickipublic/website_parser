<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 22/07/16
 * Time: 14:27
 */
interface Serviceable{
    public function getViewData($constraints=NULL);
}