<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 22/07/16
 * Time: 15:43
 */
ClassLoader::load("XMLParser", 'XMLParser');
ClassLoader::load('Serviceable', 'services', 'interface');

class hampshire_wedding_venues implements Serviceable
{
    protected $url = 'http://www3.hants.gov.uk/registration/marriage/approvedvenues.htm';

    public function getViewData($constraints = NULL)
    {
        $debug = false;
        foreach ($constraints as $paramSet) {
            if ($paramSet->param == 'debug') {
                $debug = true;
            }
        }

        $xmlParser = new XMLParser($debug);
        $xmlParser->setPage($this->url);
        $results = array();

        $this->setParseMainPageData($xmlParser);
        $mainResults = $xmlParser
            ->parseWithin('page_content')
            ->fetchResults();

        foreach ($mainResults[0] as $url) {
            $xmlParser->setPage('http://www3.hants.gov.uk/'.$url);
            $this->setParseDetailedPageData($xmlParser);
            $results[] = $xmlParser
                ->parseWithin('local_results')
                ->fetchResults();
        }

        foreach($results as &$r){
            $this->appendPhoneNumbers($r, $xmlParser);
        }

        return $this->beautify($results);
    }

    ////////////////////////////////////////////////////////////////parser configuration///////////////////////
    private function setParseMainPageData($xmlParser)
    {
        $xmlParser
            ->setEvent(FindTagsChildrenChain, array('li', 'nav', 'ul'))
            ->addAction(Collect_li_Siblings_a_href);
    }

    private function setParseDetailedPageData($xmlParser)
    {
        $xmlParser
            ->setEvent(TagAttributeValueMatching, array('tag' => 'li', 'attribute' => 'class', 'match' => '/six columns/'))
            ->addAction(HampshirePagesTagValues, array('mainUrl' => 'http://www3.hants.gov.uk/'));
    }

    private function setParsePhoneNumbers($xmlParser){
        $xmlParser
            ->setEvent(FindTagsSiblingsChain, array('h2', 'p'))
            ->addAction(ExtractTextValueFrom_p_Sibling);
    }

    private function beautify($array){
        $result = array();
        $header = array();
        foreach($array[0][0] as $k=>$a){
            $header[$k] = $k;
        }
        $result[] = $header;

        foreach($array as $area){
            foreach($area as $row){
                $pureRow = array();
                $name = '';
                foreach($row as $k=>$rec){
                    if($k == 'name'){
                        $name = $rec;
                    }
                    $pureRow[] = $rec;
                }
                $result[$name] = $pureRow;
            }
        }
        return $result;
    }

    ////////////////////////////////////////////////////////////////helpers///////////////////////
    private function appendPhoneNumbers(&$array, $parser){
        $parser->purge();

        foreach($array as &$r){
            $parser->setPage($r['registration url']);
            $this->setParsePhoneNumbers($parser);
            $phone = $parser
                        ->parseWithin('page_content')
                        ->fetchResults();
            if($phone && isset($phone[0]) && isset($phone[0][0])){
                $phone = $phone[0][0];
            }else{
                $phone = NULL;
            }
            $r['phone number'] = $phone;
        }
    }
}