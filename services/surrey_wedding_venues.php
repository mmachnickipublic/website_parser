<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 18/07/16
 * Time: 14:42
 */
ClassLoader::load("XMLParser", 'XMLParser');
ClassLoader::load('Serviceable', 'services', 'interface');

class surrey_wedding_venues implements Serviceable
{
    protected $url = 'http://www.surreycc.gov.uk/people-and-community/birth-death-marriage-and-civil-partnerships/marriage/guide-to-booking-your-marriage-or-civil-partnership-ceremony/choose-your-ceremony-venue/licensed-ceremony-venues';
    
    public function getViewData($constraints=NULL){
        $debug = false;
        foreach($constraints as $paramSet){
            if($paramSet->param == 'debug'){
                $debug = true;
            }
        }

        $xmlParser = new XMLParser($debug);
        $xmlParser->setPage($this->url);
        $results = array();

        $this->setParseMainPageData($xmlParser);
        $mainResults = $xmlParser
                            ->parseWithin('scc-main-content')
                            ->fetchResults();
        
        foreach($mainResults as $url){
            $xmlParser->setPage($url['href']);
            $this->setParseDetailedPageData($xmlParser);
            $results[] = $xmlParser
                            ->parseWithin('scc-main-content')
                            ->fetchResults();
        }        
        return $this->beautify($results);
    }

    ////////////////////////////////////////////////////////////////parser configuration///////////////////////
    private function setParseMainPageData($xmlParser){
        $xmlParser
            ->setEvent(TagAttributeValueMatching, array('tag' => 'a', 'attribute' => 'href', 'match' => '/licensed-ceremony-venues/'))
            ->addAction(Tag_a_Href);
    }

    private function setParseDetailedPageData($xmlParser){
        //covers h2 > ul > li > data
        $xmlParser
            ->setEvent(Find_h2_ul_PairTag)
            ->addAction(ExtractTextValueFromTag, 'Venue Name')
            ->addAction(CollectNextNode_ul_DataWith_a_href_Child);

        //covers p > ul > li > data
        $xmlParser
            ->setEvent(Find_p_ul_PairTag)
            ->addAction(ExtractTextValueFromTag, 'Venue Name from p')
            ->addAction(CollectNextNode_ul_DataWith_a_href_Child)
            ->addAction(Extract_a_href_FromTag);
    }

    ////////////////////////////////////////////////////////////////helpers///////////////////////
    private function beautify($data){
        $result = array();
        $header = array('Name', 'Number of licensed rooms', 'Maximum number of guests', 'Gazeboo', 'Address', 'Telephone', 'Website');
        foreach($data as $area) {
            $a = array();
            foreach ($area as $dSet) {
                $record = array();
                if (isset($dSet['Venue Name from p'])) {
                    $i = count($dSet)-1;
                    array_splice($dSet, $i-1, $i, array_reverse(array_slice($dSet, $i-1, $i)));
                }
                foreach($dSet as $r){
                    if(strpos($r, 'http') === false && strpos($r, ':') !== false){
                        $r = substr($r, strpos($r, ':')+1);
                    }
                    $record[] = trim($r);
                }
                $a[] = $record;
            }
            $result[] = $a;
        }
        return array_merge($header, $result);
    }
}